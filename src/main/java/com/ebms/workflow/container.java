package com.ebms.workflow;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class container implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label("IsAdminApprovalReq")
	private java.lang.Boolean isAdminApprovalReq;
	@org.kie.api.definition.type.Label("IsEmpApprovalReq")
	private java.lang.Boolean isEmpApprovalReq;
	@org.kie.api.definition.type.Label("IsNotificationReq")
	private java.lang.Boolean isNotificationReq;
	@org.kie.api.definition.type.Label("IsEmpApproved")
	private java.lang.Boolean isEmpApproved;
	@org.kie.api.definition.type.Label("IsAdminApproved")
	private java.lang.Boolean isAdminApproved;
	@org.kie.api.definition.type.Label("isDBUpdateReq")
	private java.lang.Boolean isDBUpdateReq;
	@org.kie.api.definition.type.Label("isEnrichmentReq")
	private java.lang.Boolean isEnrichmentReq;
	@org.kie.api.definition.type.Label(value = "isReturnToPreviousReq")
	private java.lang.Boolean isReturnToPreviousReq;
	public container() {
	}

	public java.lang.Boolean getIsAdminApprovalReq() {
		return this.isAdminApprovalReq;
	}

	public void setIsAdminApprovalReq(java.lang.Boolean isAdminApprovalReq) {
		this.isAdminApprovalReq = isAdminApprovalReq;
	}

	public java.lang.Boolean getIsEmpApprovalReq() {
		return this.isEmpApprovalReq;
	}

	public void setIsEmpApprovalReq(java.lang.Boolean isEmpApprovalReq) {
		this.isEmpApprovalReq = isEmpApprovalReq;
	}

	public java.lang.Boolean getIsNotificationReq() {
		return this.isNotificationReq;
	}

	public void setIsNotificationReq(java.lang.Boolean isNotificationReq) {
		this.isNotificationReq = isNotificationReq;
	}

	public java.lang.Boolean getIsEmpApproved() {
		return this.isEmpApproved;
	}

	public void setIsEmpApproved(java.lang.Boolean isEmpApproved) {
		this.isEmpApproved = isEmpApproved;
	}

	public java.lang.Boolean getIsAdminApproved() {
		return this.isAdminApproved;
	}

	public void setIsAdminApproved(java.lang.Boolean isAdminApproved) {
		this.isAdminApproved = isAdminApproved;
	}

	public java.lang.Boolean getIsDBUpdateReq() {
		return this.isDBUpdateReq;
	}

	public void setIsDBUpdateReq(java.lang.Boolean isDBUpdateReq) {
		this.isDBUpdateReq = isDBUpdateReq;
	}

	public java.lang.Boolean getIsEnrichmentReq() {
		return this.isEnrichmentReq;
	}

	public void setIsEnrichmentReq(java.lang.Boolean isEnrichmentReq) {
		this.isEnrichmentReq = isEnrichmentReq;
	}

	public java.lang.Boolean getIsReturnToPreviousReq() {
		return this.isReturnToPreviousReq;
	}

	public void setIsReturnToPreviousReq(java.lang.Boolean isReturnToPreviousReq) {
		this.isReturnToPreviousReq = isReturnToPreviousReq;
	}

	public container(java.lang.Boolean isAdminApprovalReq,
			java.lang.Boolean isEmpApprovalReq,
			java.lang.Boolean isNotificationReq,
			java.lang.Boolean isEmpApproved, java.lang.Boolean isAdminApproved,
			java.lang.Boolean isDBUpdateReq, java.lang.Boolean isEnrichmentReq,
			java.lang.Boolean isReturnToPreviousReq) {
		this.isAdminApprovalReq = isAdminApprovalReq;
		this.isEmpApprovalReq = isEmpApprovalReq;
		this.isNotificationReq = isNotificationReq;
		this.isEmpApproved = isEmpApproved;
		this.isAdminApproved = isAdminApproved;
		this.isDBUpdateReq = isDBUpdateReq;
		this.isEnrichmentReq = isEnrichmentReq;
		this.isReturnToPreviousReq = isReturnToPreviousReq;
	}

}